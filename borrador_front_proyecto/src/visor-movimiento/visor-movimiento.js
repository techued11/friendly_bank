import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
/**
 * @customElement
 * @polymer
 */
class VisorMov extends PolymerElement {

  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link rel="stylesheet" href="css/estilos.css">

      <div id="table2-texto">
      <dom-repeat items="{{mov}}">
       <template>
         <table class="table-sm table-hover table-bordered table-light">
           <caption>Movimientos</caption>
           <thead>
             <tr>
               <th scope="col">Movimiento</th>
               <th scope="col">Tipo</th>
               <th scope="col">Valor</th>
               <th scope="col">Cuenta</th>
             </tr>
           </thead>
           <tbody>
             <tr>
               <td>{{item.IdMovimiento}}</td>
               <td>{{item.Tipo}}</td>
               <td>{{item.Valor}}</td>
               <td>{{item.IdCuenta}}</td>
             </tr>
           </tbody>
         </table>
       </template>
      </dom-repeat>
      </div>

      <iron-ajax
      id="getMovement"
      url="http://localhost:3000/apitechu/v2/movements/{{id}}"
      handle-as="json"
      method="GET"
      on-response="manageAJAXResponse"
      on-error="showError"
      ></iron-ajax>
    `;
  }

  static get properties() {
    return {
      id : {
        type: Number,
        observer: "_useridChanged"
      }, mov: {
        type: Array
      }
    };
  } //end properties


showError(error){
    console.log("Hubo un error");
    console.log(error);
}

manageAJAXResponse(data){
  console.log("manageAJAXResponse");
  console.log(data.detail.response);
  this.mov = data.detail.response;
}
_useridChanged() {
  console.log("estoy pasando por useridChanged");
  this.$.getMovement.generateRequest();
}
}//End class


window.customElements.define('visor-movimiento', VisorMov);
