import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
/**
 * @customElement
 * @polymer
 */
class LogoutUsuario2 extends PolymerElement {

  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link rel="stylesheet" href="css/estilos.css">
      

      <iron-ajax
      id="logoutId"
      url="http://localhost:3000/apitechu/v2/logout/{{id}}"
      handle-as="json"
      method="POST"
      on-response="manageAJAXResponse"
      on-error="showError"
      ></iron-ajax>
    `;
  }

  static get properties() {
    return {
      id : {
        type: Number,
        observer: "_useridChanged"
      }, mov: {
        type: String
      },
      unLogged:{
        type: Boolean,
        value:false
      }
    };
  } //end properties


showError(error){
    console.log("Hubo un error");
    console.log(error);
}

manageAJAXResponse(data){
  console.log("manageAJAXResponse de LOGOUT");
  console.log("IMPRIMIENDO RESPUESTA de LOGOUT");
  console.log(data.detail.response);
  this.mov=data.detail.response.msg;
  if (data.detail.response.msg == "Propiedad Logged quitada al usuario"){
    this.unLogged = true;
    console.log("LOGOUT ok");

  }
  else {
    this.unLogged = false;
  }
}
_useridChanged() {
  console.log("estoy pasando por useridChanged");
  this.$.logoutId.generateRequest();
}
}//End class



window.customElements.define('logout-usuario2', LogoutUsuario2);
