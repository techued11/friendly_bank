import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '../visor-ingreso/visor-ingreso.js';

/**
 * @customElement
 * @polymer
 */
class VisorCuenta extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link rel="stylesheet" href="css/estilos.css">
      <div hidden="{{oculto::input}}" id="AccountList"  class="form-group">
          <button on-click="VerCuentas" type="button" class="btn btn-primary btn-lg-sm">Ver Cuentas</button>
          <button hidden$="{{!isLogged}}" on-click="showMovs" type="button" class="btn btn-primary btn-lg-sm">Movimientos</button>
      </div>
      <visor-movimiento hidden$="{{viendomov}}" id="visorMov"></visor-movimiento>

        <div id="table-texto" hidden$="{{!viendomov}}">
          <dom-repeat items="{{accounts}}">
            <template>
              <table class="table-sm table-hover table-bordered table-light">
              <caption>Cuentas</caption>
                <thead>
             <tr>
               <th scope="col">IBAN</th>
               <th scope="col">Saldo</th>
               <th scope="col">IdCuenta</th>
             </tr>
              </thead>
           <tbody>
             <tr>
               <td>{{item.IBAN}}</td>
               <td>{{item.Saldo}}</td>
               <td>{{item.IdCuenta}}</td>
             </tr>
           </tbody>
         </table>
       </template>
      </dom-repeat>
      </div>

      <iron-ajax
      id="getAccounts"
      url="http://localhost:3000/apitechu/v2/accounts/{{id}}"
      handle-as="json"
      method="GET"
      on-response="manageAJAXResponse"
      on-error="showError"
      ></iron-ajax>
    `;
  }
  static get properties() {
    return {
      id : {
        type: Number
      }, accounts: {
        type: Array
      },
      oculto:{
        type: Boolean,
        value: true
      },
      IdCuenta:{
        type: Boolean,
        value: true
      },isLogged:{
        type: Boolean,
        value:false
      }, viendomov: {
        type: Boolean,
        value:true
      }

    };
  } //end properties


showError(error){
    console.log("Hubo un error");
    console.log(error);
}

showMovs(){

    this.$.visorMov.id=this.IdCuenta;
    console.log(this.$.visorMov.id);
    this.viendomov = false;
  }

manageAJAXResponse(data){
  console.log("manageAJAXResponse");
  console.log(data.detail.response);
  this.accounts = data.detail.response;
  this.IdCuenta = data.detail.response[0].IdCuenta;
  this.isLogged = true;
}
_useridChanged() {
  console.log("estoy pasando por useridChanged");
  this.$.getAccounts.generateRequest();
}

VerCuentas(data){
  console.log("VerCuentas");
  this.$.getAccounts.generateRequest();
  this.viendomov = true;
}

}//End class

window.customElements.define('visor-cuenta', VisorCuenta);
