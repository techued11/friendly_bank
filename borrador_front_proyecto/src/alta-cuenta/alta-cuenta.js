import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
/**
 * @customElement
 * @polymer
 */
class AltaCuenta extends PolymerElement {

  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link rel="stylesheet" href="css/estilos.css">
      <br><br/>

      <form class="form-inline justify-content-center">
        <div hidden="{{oculto::input}}" id="InfoAccount"  class="form-group">
          <strong><div id="mensaje-texto">
            <button hidden$="{{!isLogged}}" on-click="CrearCuenta" type="button" class="btn btn-primary btn-lg-sm">Crear Cuenta</button>
          </strong></div>
        </div>
      <form>
      <br><br/>


      <iron-ajax
      id="doAccount"
      url="http://localhost:3000/apitechu/v2/accounts"
      handle-as="json"
      method="POST"
      content-type= "application/json"
      on-response="manageAJAXResponse"
      on-error="showError"
      ></iron-ajax>
    `;

  }

  static get properties() {
    return {
      id:{
        type: Number
      },
      cantidad:{
        type: Number,
        value: 0
      },
      oculto:{
        type: Boolean,
        value: true
      },veralta:{
        type: Boolean,
        value:false
      },isLogged:{
        type: Boolean,
        value:true
      }
    };
  } //end properties


showError(error){
    console.log("Hubo un error creando cuenta");
    console.log(error);
}

manageAJAXResponse(data){
  console.log("manageAJAXResponse de CREAR CUENTA");
  console.log("IMPRIMIENDO RESPUESTA de CREAR CUENTA");
  console.log(data.detail.response);
  //this.$.AltaCuenta.hidden = false;
  this.isLogged = false;

}

CrearCuenta(data){
  console.log("CrearCuenta");

  var newAcc ={
    "Nombre": "Cuenta Nomina",
    "Saldo": parseInt(this.cantidad),
    "UserId" : parseInt(this.id),
    "IBAN" : "BR40 6473 8LAH BOJL 34ME PIZU 12"
  }

  console.log("IMPRIMIENTO DATOS DE LA CUENTA A CREAR");
  console.log(newAcc);

  this.$.doAccount.body = JSON.stringify(newAcc);  //lo trasnformo en json los datos
  console.log("---- IMPRIMO EL BODY DEL CREAR CUENTA ----");
  console.log(this.$.doAccount.body);

  this.$.doAccount.generateRequest();

}

}//End class



window.customElements.define('alta-cuenta', AltaCuenta);
