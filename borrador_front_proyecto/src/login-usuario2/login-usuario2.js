import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '../visor-ingreso/visor-ingreso.js';


/**
 * @customElement
 * @polymer
 */
class LoginUsuario2 extends PolymerElement {
  static get template() {
    return html`
    <style>
            :host {
              display: block;
            }

    </style>

          <link rel="stylesheet" href="css/estilos.css">
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
          
          <div id="tittle-texto">
          <div class="row">
          <div id="panelLogin" class="col-sm-12">
                <br></br><h1 class="display-3">Friendly Bank</h1>
                <p>Bienvenido al banco del futuro, tu banco 7.0</p> </br></br>
                <span hidden$="{{!isLogged}}">Bienvenido/a de nuevo</span>
                <form class="form-inline justify-content-center">
                    <div class="form-group">
                        <label class="sr-only">usuario</label>
                        <input id = "inputemail" type="text" class="form-control form-control-lg-sm" placeholder="email" value="{{email::input}}"></input>
                    </div>
                    <div class="form-group">
                        <label class="sr-only">contraseña</label>
                        <input id = "inputpassword" type="password" class="form-control form-control-lg-sm" placeholder="password" value="{{password::input}}"></input>
                    </div>
                    <button id = "buttonlogin" on-click="login" type="button" class="btn btn-secondary btn-lg-sm">Login</button>
                </form>
            </div>
                  </form>
                </div>
            </div>
          </div>


      <iron-ajax
      id="doLogin"
      url="http://localhost:3000/apitechu/v2/login"
      handle-as="json"
      method="POST"
      content-type="application/json"
      on-response="manageAJAXResponse"
      on-error="showError"
      ></iron-ajax>
    `;
  }

  static get properties() {
    return {

      email: {
        type: String
      },
      password:{
        type: String
      },
      id:{
        type: Number,
        observer: '_useridChanged'
      },
      isLogged:{
        type: Boolean,
        value:false
      }
    };
  } //end properties

login() {
  console.log("El usuario ha pulsado el boton login");
  console.log("Voy a enviar la peticion del login ");

  var loginData = {
    "email" : this.email,
    "password" : this.password
  }

  console.log("BODY A ENVIAR EN EL LOGIN");
  console.log(loginData);
  this.$.doLogin.body = JSON.stringify(loginData);
  this.$.doLogin.generateRequest();

  console.log("Peticion de login enviada");
}

showError(error){
    console.log("Hubo un error en el login");
    console.log(error);
}

manageAJAXResponse(data){
  console.log("manageAJAXResponse del Login");
  console.log("Imprimimos respuesta del Login");
  console.log(data.detail.response);
  if (data.detail.response.msg == "Usuario con propiedad logged"){
    this.isLogged = true;
    console.log("Login ha sido OK");
    console.log(this.isLogged);
  }
  else {
    console.log("Login ha sido KO");
    this.isLogged = false;
  }
  if (this.isLogged) {
     console.log("Lanzamos evento y mostramos el UserId");
     console.log(data.detail.response.UserId);
     this.$.buttonlogin.hidden = true;
     this.$.inputemail.hidden = true;
     this.$.inputpassword.hidden = true;

     this.dispatchEvent(
       new CustomEvent(
         "loginsucess",{
           "detail" : {
             "id" : data.detail.response.UserId
           }
         }
       )
     )
  }
}
}//End class

window.customElements.define('login-usuario2', LoginUsuario2);
