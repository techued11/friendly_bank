import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

/**
 * @customElement
 * @polymer
 */
class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }

        .redbg{
          background-color:red;
        }

        .bluebg{
          background-color:blue;
        }

        .greybg{
          background-color:grey;
        }

        .greenbg{
          background-color:green;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h2>[[first_name]] [[last_name]]</h2>
      <h3>[[email]]</h3>

      <iron-ajax
      auto
      id="getUser"
      url="http://localhost:3000/apitechu/v2/users/{{id}}"
      handle-as="json"
      on-response="showData"
      ></iron-ajax>
    `;
  }
  static get properties() {
    return {
      first_name: {
        type: String,
      }, last_name:{
        type: String,
      }, email: {
        type: String
      }, id:{
        type: Number
      }
    };
  } //end properties

showData(data){
  console.log("showData");
  console.log("Imprimiendo respuesta del getUser");
  console.log(data.detail.response);
  this.first_name=data.detail.response.first_name;
  this.last_name=data.detail.response.last_name;
  this.email=data.detail.response.email;
}

}//End class

window.customElements.define('visor-usuario', VisorUsuario);
