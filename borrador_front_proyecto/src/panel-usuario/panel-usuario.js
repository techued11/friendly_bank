import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '../login-usuario2/login-usuario2.js'
import '../visor-usuario2/visor-usuario2.js'


/**
 * @customElement
 * @polymer
 */
class PanelUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

     <login-usuario2 on-loginsucess="processEvent"></login-usuario2>
     <visor-usuario2 id="receiver"></visor-usuario2>
    
   `;
 }

 static get properties() {
   return {
   };
 }

 processEvent(e) {
   console.log("Capturado evento del emisor login-usuario2 (UserId)");
   console.log(e.detail.id);

   this.$.receiver.id = e.detail.id;
   console.log(this.$.receiver.id);
 }
}//End class

window.customElements.define('panel-usuario', PanelUsuario);
