import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

/**
 * @customElement
 * @polymer
 */
class ReceptorEvento extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h3>Soy el receptor</h3>
      <h4>Este curso es [[course]]</h4>
      <h4>y estamos en el año [[year]]</h4>
      <input type="text" value="{{course::input}}"></input>


    `;
  }

  static get properties() {
    return {
      course: {
        type : String,
        observer : "_courseChanged"
      },
      year : {
        type : String
      }
    };
  } //end properties


_courseChanged(newValue,oldValue) {
   console.log("Course value has changed");
   console.log("Old value was " + oldValue);
   console.log("New value is " + newValue);
}

sendEvent(e) {
  console.log("El usuario ha pulsado el boton");
  console.log(e);

  this.dispatchEvent(
    new CustomEvent(
      "myevent",{
        "detail" : {
          "course" : "TechU",
          "year" : 2019
        }
      }
    )
  )

}



}//End class

window.customElements.define('receptor-evento', ReceptorEvento);
