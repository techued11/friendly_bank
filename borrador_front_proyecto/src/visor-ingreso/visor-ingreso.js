import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class VisorIng extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          }
    </style>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/estilos.css">

      <form class="form-inline justify-content-center">
        <!--  <div class="form-group">
              <input type="Tipo" class="form-control form-control-lg" placeholder="Reintegro o Ingreso" value="{{Tipo::input}}"></input>
          </div> -->
          <select class="form-control form-control-lg-sm" id="TipoMovimiento">
             <option value="Ingreso" selected>Ingreso</option>
             <option value="Reintegro">Reintegro</option>
          </select>

          <div class="form-group">
              <input type="Valor" class="form-control form-control-lg-sm" placeholder="Cantidad" value="{{Valor::input}}"></input>
          </div>
          <div class="form-group">
              <input type="IdCuenta" class="form-control form-control-lg-sm" placeholder="Cuenta destino" value="{{IdCuenta::input}}"></input>
          </div>
      </form>
      <button on-click="Ingresar" type="button" class="btn btn-primary btn-lg-sm">Ingreso/Reintegro</button>
    <br></br>
    <div id="mensaje-texto">
    <div id="DineroIngresado" hidden="true">
      <e> La operación se ha realizado correctamente</e>
    </div>
    </div>

     <span hidden$="{{!existe}}"> NO se ha podido realizar el ingreso</span>


    <iron-ajax
       id="doIngreso"
       url="http://localhost:3000/apitechu/v2/movements/{{IdCuenta}}"
       handle-as="json"
       method ="POST"
       content-type= "application/json"
       on-response="manageAJAXResponse"
       on-error="showError"
    ></iron-ajax>
     `;
   }

   static get properties() {
     return {
       Tipo: {
         type: String
       },Valor:{
         type:Number
       },IdCuenta:{
         type: Number,
         observer: '_cuentaidChanged'
       },isLogged:{
         type:Boolean,
         value:false
       },existe:{
         type:Boolean,
         value:false
       }
     };
   } // Fin de properties

  _cuentaidChanged(newValue, oldValue) {
     //this.$.doIngreso.generateRequest();
   }

  Ingresar(){
  console.log("El Usuario ha pulsado el boton de Ingresar");
  console.log("voy a enviar la peticion para HACER INGRESO");

    var newIng ={
      "Tipo": this.$.TipoMovimiento.value,
      "Valor": parseInt(this.Valor)
    }

    console.log("IMPRIMIENTO DATOS INGRESO");
    console.log(newIng);

    this.$.doIngreso.body = JSON.stringify(newIng);  //lo trasnformo en json los datos
    this.$.doIngreso.generateRequest();
    console.log ("Peticion de ingreso enviada");
  }

  showError(error){
  console.log("Hubo un error en el ingresoc");
  console.log(error);
  }

  manageAJAXResponse(data){
     console.log("manageAJAXResponse de INGRESO");
     console.log("IMPRIMIENDO RESPUESTA INGRESAR DINERO")
     console.log(data.detail.response);
     this.$.DineroIngresado.hidden = false;

  }
  }  //End de la clase
window.customElements.define('visor-ingreso', VisorIng);
