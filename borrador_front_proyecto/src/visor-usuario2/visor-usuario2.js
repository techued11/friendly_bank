import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '../visor-cuenta/visor-cuenta.js';
import '../visor-movimiento/visor-movimiento.js';
import '../logout-usuario2/logout-usuario2.js';
import '../visor-ingreso/visor-ingreso.js';
import '../alta-usuario/alta-usuario.js';
import '../alta-cuenta/alta-cuenta.js';
/**
 * @customElement
 * @polymer
 */
class VisorUsuario2 extends PolymerElement {
  static get template() {
    return html`
    <style>
       :host {
         display: block;
       }
     </style>
     <link rel="stylesheet" href="css/estilos.css">
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <strong><div id="mensaje-texto">
          <blockquote class="blockquote text-center">
          <p id="msgDespedida" hidden = "true" class="mb-2">¡¡Hasta la proxima [[first_name]] [[last_name]]!! </p>
            <footer class=></footer>
          </blockquote>
     </strong></div>

     <strong><div id="mensaje-texto">
       <blockquote class="blockquote text-center">
         <p id="msgBienvenida" class="cover-texto">[[msg1]] [[first_name]] [[last_name]][[msg2]][[msg3]][[email]]</p>
         <footer class=></footer>
       </blockquote>
     </strong></div>
     <button hidden$="{{!isLogged}}" id="buttonNewAccount" on-click="showNewAccount" type="submit" class="btn btn-success btn-lg-sm">Cuentas</button><br/> <br/>
     <button hidden$="{{!isLogged}}" id="buttonIngreso" on-click="showIngreso" type="submit" class="btn btn-success btn-lg-sm">Ingreso/Reintegro</button> <br/> <br/>
     <button hidden$="{{!isLogged}}" id = "buttonLogout" on-click="showLogout" type="button" class="btn btn-secondary btn-lg-sm">Logout</button>
     <div id="cover-texto">
        <a hidden$="{{isLogged}}" href="#AltaUsuario" on-click="showAltaUsuario">¿No eres cliente? Registrate</a>

        <visor-cuenta hidden$="{{!vercuenta}}" id="visorCuenta"></visor-cuenta>
        <logout-usuario2 id="logOut"></logout-usuario2>
        <visor-ingreso hidden$="{{!veringreso}}"></visor-ingreso>
        <alta-cuenta hidden$="{{!altacuenta}}" id="newAccount"></alta-cuenta>
      </div>
      <alta-usuario hidden$="{{!veralta}}" id="AltaUsuario" ></alta-usuario>

    <!--  <div id="AltaUsuarioXXXX" hidden="true">
         <input id = "inputFirstName" type="text" class="form-control form-control-lg" placeholder="Nombre"></input>
         <input id = "inputLastName" type="text" class="form-control form-control-lg" placeholder="Apellidos"></input>
         <input id = "inputEmail" type="text" class="form-control form-control-lg" placeholder="email"></input>
         <input id = "inputPassword" type="password" class="form-control form-control-lg" placeholder="password"></input>
         <button on-click="newUser" type="submit" class="btn btn-success btn-lg">Nuevo Usuario</button>
      </div> -->

          <!--  <a href="#AltaIngreso" on-click="showIngreso">Realizar Ingreso</a>-->
  </div>

   <iron-ajax
     auto
     id="getUser"
     url="http://localhost:3000/apitechu/v2/users/{{id}}"
     handle-as="json"
     on-response="showData">
   </iron-ajax>
 `;
}
static get properties() {
  return {
    first_name: {
      type: String,
    }, last_name:{
      type: String,
    }, email: {
      type: String
    }, id:{
      type: Number
    }, msg1: {
      type: String
    }, msg2: {
      type: String
    }, msg3: {
      type: String
    }, msg: {
      type: String
    },isLogged:{
      type: Boolean,
      value:false
    },IdCuenta:{
      type: Number
    },veringreso:{
      type: Boolean,
      value:false
    },vercuenta:{
      type: Boolean,
      value:false
    },isAlta:{
      type: Boolean,
      value:false
    },veralta:{
      type: Boolean,
      value:false
    },altacuenta:{
      type: Boolean,
      value:false
    }
  };
} //end properties


showData(data){
console.log("showData");
console.log("IMPRIMIENDO RESPUESTA GETUSER");
console.log(data.detail.response);
this.id=data.detail.response.UserId;
this.first_name=data.detail.response.first_name;
this.last_name=data.detail.response.last_name;
this.email=data.detail.response.email;
this.msg=data.detail.response.msg;
this.msg1="¡Bienvenido "
this.msg2=" !"
this.msg3=" Te has logado con usuario: "
this.isLogged = true;
this.veralta =false;
}

showAltaUsuario(data){
console.log("showAltaUsuario");
//this.$.AltaUsuario.hidden = false;
//this.$.msgDespedida.hidden = true;
this.veralta = true;

}

showIngreso(data){
console.log("showIngreso");
this.veringreso = true;
this.vercuenta = false;
this.$.newAccount.oculto = true;
//this.$.msgDespedida.hidden = true;

}

_useridChanged(newValue, oldValue) {
this.$.getUser.generateRequest();
}

showAccounts(){
console.log("valor que falla: ")
console.log(this.id)
console.log("que le paso a visor de  cuenta");
this.$.visorCuenta.id = this.id;
console.log(this.$.visorCuenta.id);
}



showLogout(){
  console.log("IMPRIMIR USUARIO PARA LOGOUT")
  console.log(this.id)
  console.log("visor de  logout");
  this.$.logOut.id = this.id;
  console.log(this.$.logOut.id);
  this.$.msgBienvenida.hidden = true;
  this.$.buttonLogout.hidden = true;
  this.$.msgDespedida.hidden = false;
  this.isLogged = false;
  this.vercuenta = false;
  this.AltaUsuario = false;
  this.veringreso = false;
  this.isAlta = false;
}
showNewAccount(){
    console.log("IMPRIMIR USUARIO PARA CREAR CUENTA");
    console.log(this.id);
    console.log(this.$.newAccount.oculto);
    this.$.newAccount.id = this.id;
    this.$.newAccount.oculto = false;
    console.log(this.$.newAccount.oculto);
    this.$.visorCuenta.id = this.id;
    this.$.visorCuenta.oculto = false;
    this.veringreso = false;
    this.vercuenta = true;
    this.altacuenta =true;
  }

}//End class

window.customElements.define('visor-usuario2', VisorUsuario2);
