import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * @customElement
 * @polymer
 */
class Borrador_front_proyectoApp extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <h2>Hello [[prop1]]!</h2>
    `;
  }
  static get properties() {
    return {
      prop1: {
        type: String,
        value: 'borrador_front_proyecto-app'
      }
    };
  }
}

window.customElements.define('borrador_front_proyecto-app', Borrador_front_proyectoApp);
