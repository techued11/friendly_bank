import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
/**
 * @customElement
 * @polymer
 */
class AltaUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          }
    </style>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/estilos.css">
    <br><br>
    <br><br>
      <div id="cover-texto">
      </div>
        <form class="form-inline justify-content-center">
            <div class="form-group">
                <input type="first_name" class="form-control form-control-lg-sm" placeholder="Nombre" value="{{first_name::input}}"></input>
            </div>
            <div class="form-group">
                <input type="last_name" class="form-control form-control-lg-sm" placeholder="Apellido" value="{{last_name::input}}"></input>
            </div>
            <div class="form-group">
                <input type="email" class="form-control form-control-lg-sm" placeholder="Email" value="{{email::input}}"></input>
            </div>
            <div class="form-group">
                <input type="password" class="form-control form-control-lg-sm" placeholder="Contraseña" value="{{password::input}}"></input>
            </div>
          <button on-click="Registro" type="button" class="btn btn-primary btn-lg-sm">Usuario nuevo</button>
        </form>

      <div id="mensaje-texto">
      <div id="MsgUserCreado" hidden="true">
        <e>Usuario creado!!</e>
      </div>
    </div>
    </div>
<!--     <button on-click="creacuenta">Crear cuenta nueva </button>  -->

     <span hidden$="{{!existe}}"> NO se ha podido dar de alta el correo indicado</span>


    <iron-ajax
       id="doRegistro"
       url="http://localhost:3000/apitechu/v2/users"
       handle-as="json"
       method ="POST"
       content-type= "application/json"
       on-response="manageAJAXResponse"
       on-error="showError"
    ></iron-ajax>
     `;
   }

   static get properties() {
     return {
       first_name: {
         type: String
       },last_name:{
         type:String
       },email: {
         type: String
       },password:{
         type:String
       },isLogged:{
         type:Boolean,
         value:false
       },existe:{
         type:Boolean,
         value:false
       }
     };
   } // Fin de properties

 Registro(){
  console.log("El Usuario ha pulsado el boton de Registro");
  console.log("voy a enviar la peticion para CREAR USUARIO");

    var newUser ={
      "first_name": this.first_name,
      "last_name": this.last_name,
      "email" : this.email,
      "password" : this.password
    }

    console.log("IMPRIMIENTO DATOS USUARIO A CREAR");
    console.log(newUser);

    this.$.doRegistro.body = JSON.stringify(newUser);  //lo trasnformo en json los datos
    this.$.doRegistro.generateRequest();
    console.log ("Peticion de registro de nuevo usuario enviada");
 }

showError(error){
  console.log("Hubo un error en el resgistro del usuario");
  console.log(error);
 }

manageAJAXResponse(data){
     console.log("manageAJAXResponse del CREAR USUARIO");
     console.log("IMPRIMIENDO RESPUESTA CREAR USUARIO")
     console.log(data.detail.response);
     this.$.MsgUserCreado.hidden = false;

  }
}  //End de la clase


window.customElements.define('alta-usuario', AltaUsuario);
