const requestJson = require('request-json');
const io = require ('../io');
const crypt = require ('../crypt');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuaag11ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function getAccountById (req, res) {
  console.log("GET /apitechu/v2/accounts/:id");
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente httpClient creado");

  var UserId = req.params.id;
  var query = 'q={"UserId":' + UserId + '}';

  console.log("La consulta es " + query);

  /*httpCllient.get("user?" + query + "&" + mLabAPIKey,
    function (err, resMLab, body){
    var response = !err ? body [0] : {
      "msg" : "Error obteniendo usuario"
    }
    res.send(response);
  }*/
  //montar la URL que queremos mandar


  httpClient.get("account?" + query + "&" + mLabAPIKey,
    function (err, resMLab, body){
      if (err){
          var response =  {
            "msg" : "Error obteniendo cuentas"
          }
          res.status(500);
        } else {
          if(body.length > 0){
            var response = body;
          } else {
            var response = {
              "msg" : "Cuenta no encontrado"
                }
            res.status(404);
           }
       }
  res.send(response);
}
  )
}

function createAccount (req, res) {
  console.log("POST /apitechu/v2/accounts");
  console.log("IdCuenta es " + req.body.IdCuenta);
  console.log("Nombre es " + req.body.Nombre);
  console.log("Saldo es " + req.body.Saldo);
  console.log("UserId es " + req.body.UserId);
  console.log("IBAN es  " + req.body.IBAN);

  httpClient = requestJson.createClient(baseMLabURL);

  // RECUPERAMOS LA ULTIMA CUENTA DE LA COLECCION
  var query = 's={"IdCuenta":-1}&l=1';
  console.log("---- QUERY ----    " + query);

  httpClient.get("account?" + query + "&" + mLabAPIKey,
    function (err, resMLab, body){

      if (err){
          var response =  {
            "msg" : "Error obteniendo los IdCuenta"
          }
          res.status(500);
        }
        else {
          if(body.length > 0){

            var IdCuenta = parseInt(body[0].IdCuenta);
            IdCuenta = IdCuenta + 1;

            console.log("---- VALOR DE LA SECUENCIA DE CUENTA ---- ");
            console.log(IdCuenta);

            var newAccount = {
              "IdCuenta" : IdCuenta,
              "Nombre" : req.body.Nombre,
              "Saldo" : req.body.Saldo,
              "UserId" : req.body.UserId,
              "IBAN" : req.body.IBAN
            }

            console.log("--------- DATOS DE LA CUENTA A CREAR ---------");
            console.log(newAccount);


            httpClient.post("account?" + mLabAPIKey, newAccount,
              function(err,resMLab,body){
                console.log("Cuenta guardada");
                res.status(201).send ({"msg" : "Cuenta creada"});
                 }
            )
       }
    }
   }
 )
}

function createAccountToUser (UserId) {
  console.log("------ CREACION DE CUENTA PARA USUARIO ------");


  var newAccount = {
    "IdCuenta" : 22,
    "Nombre" : "Cuenta Ahorro",
    "Saldo" : 0,
    "UserId" : UserId,
    "IBAN" : "JL20 6343 2TAH PSJL 37KE WFJP 56"
  }
  console.log("--------- DATOS DE LA CUENTA A CREAR ---------");
  console.log(newAccount);

  httpClient = requestJson.createClient(baseMLabURL);
  httpClient.post("account?" + mLabAPIKey, newAccount,
    function(err,resMLab,body){
      console.log("Cuenta creada para el usuario");
      //sres.status(201).send ({"msg" : "Cuenta creada"});
  }
)
}

function getMovementsByIdV2 (req, res) {
  console.log("GET /apitechu/v2/movements/:id");
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente httpClient creado");

  var IdCuenta = req.params.id;
  var query = 'q={"IdCuenta":' + IdCuenta + '}';

  console.log("La consulta es " + query);

  /*httpCllient.get("user?" + query + "&" + mLabAPIKey,
    function (err, resMLab, body){
    var response = !err ? body [0] : {
      "msg" : "Error obteniendo usuario"
    }
    res.send(response);
  }*/
  //montar la URL que queremos mandar
  httpClient.get("movement?" + query + "&" + mLabAPIKey,
    function (err, resMLab, body){
      if (err){
          var response =  {
            "msg" : "Error obteniendo movimientos"
          }
          res.status(500);
        }
        else {
           var response = body;
        }
  res.send(response);
}
  )
}

function getMovementsV2 (req, res) {
  console.log("GET /apitechu/v2/movements");
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente httpClient creado");
  //montar la URL que queremos mandar
  httpClient.get("movement?" + mLabAPIKey,
    function (err, resMLab, body){
    var response = !err ? body : {
      "msg" : "Error obteniendo movimientos"
    }
    res.send(response);
    }
  )
}


function createMovementsV1 (req, res) {
  console.log("POST /apitechu/v1/movements/:id");


  var newMovement = {
    "IdMovimiento" : 51,
    "Tipo" : req.body.Tipo,
    "Valor" : req.body.Valor,
    "IdCuenta" : parseInt(req.params.id)
  }
console.log("--------- DATOS DEL MOVIMIENTO A CREAR ---------");
  console.log(newMovement);

  httpClient = requestJson.createClient(baseMLabURL);
  httpClient.post("movement?" + mLabAPIKey, newMovement,
    function(err,resMLab,body){
      console.log("Movimiento guardado");
      res.status(201).send ({"msg" : "Movimiento creado"});
  }
)
}

function createMovementsV2 (req, res) {
  console.log("POST /apitechu/v2/movements/:id");

  httpClient = requestJson.createClient(baseMLabURL);

  // RECUPERAMOS EL ULTIMO MOVIMIENTO DE LA COLECCION
  var query = 's={"IdMovimiento":-1}&l=1';
  console.log("---- QUERY ----    " + query);

  httpClient.get("movement?" + query + "&" + mLabAPIKey,
    function (err, resMLab, body){

      if (err){
          var response =  {
            "msg" : "Error obteniendo los IdMovimiento"
          }
          res.status(500);
        }
        else {
          if(body.length > 0){

            var IdMovimiento = parseInt(body[0].IdMovimiento);
            IdMovimiento = IdMovimiento + 1;

            console.log("---- VALOR DE LA SECUENCIA DE MOVIMIENTO ---- ");
            console.log(IdMovimiento);

            var newMovement = {
              "IdMovimiento" : IdMovimiento,
              "Tipo" : req.body.Tipo,
              "Valor" : req.body.Valor,
              "IdCuenta" : parseInt(req.params.id)
            }
            console.log("--------- DATOS DEL MOVIMIENTO A CREAR ---------");
            console.log(newMovement);

            httpClient.post("movement?" + mLabAPIKey, newMovement,
               function(err,resMLab,body){
                   console.log("Movimiento guardado");

                   var query = 'q={"IdCuenta":' + req.params.id + '}';
                   console.log(" ----  RECUPERAMOS EL SALDO DE LA CUENTA ----")
                   console.log("La consulta es " + query);

                   httpClient.get("account?" + query + "&" + mLabAPIKey,
                      function (err, resMLab, body){
                          if (err){
                            var response =  {
                                  "msg" : "Error obteniendo los datos cuenta"
                            }
                            res.status(500);
                            } else {
                            if(body.length > 0){

                                var Saldo = parseInt(body[0].Saldo);
                                Saldo = Saldo + req.body.Valor;

                                var putBody = '{"$set":{"Saldo":' + Saldo + '}}';
                                console.log(putBody);
                                console.log("La consulta es otra vez " + query);

                                httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                                    function (err, resMLab, body){
                                      if(err){
                                        var response = {
                                           "msg" : "Error actualizando saldo de cuetna"
                                         }
                                        res.status(500);
                                    } else {
                                       var response = {
                                            "msg" : "Saldo actualizado"
                                         }
                                }

                         }
                     )

              } else {
                var response = {
                  "msg" : "Cuenta no encontrada"
                    }
                //res.status(404);
               }
           }
         }
        )
      }
     )
    }

   }
   res.status(201).send ({"msg" : "Movimiento creado"});
  }
 )
}




function deleteMovementsV2 (req, res) {
  console.log("DELETE /apitechu/v2/movements/:id");
  var httpClient = requestJson.createClient(baseMLabURL);

  var IdMovimiento = req.params.id;
  var query = 'q={"IdMovimiento":' + IdMovimiento + '}';

  console.log("La consulta es " + query);
  var putBody=[];
  httpClient.put("movement?" + query + "&" + mLabAPIKey, putBody,
  function (err, resMLab, body){
    if(err){
      var response = {
        "msg" : "Error borrando movimiento"
      }
      res.status(500);
    } else {
      var response = {
        "msg" : "Movimiento borrado " + IdMovimiento
      }
    }
    res.send(response);
  }
  )
}







function deleteAccountsV2 (req, res) {
  console.log("DELETE /apitechu/v2/accounts/:id");
  var httpClient = requestJson.createClient(baseMLabURL);

  var IdCuenta = req.params.id;
  var query = 'q={"IdCuenta":' + IdCuenta + '}';

  console.log("La consulta es " + query);
  var putBody=[];
  httpClient.put("account?" + query + "&" + mLabAPIKey, putBody,
  function (err, resMLab, body){
    if(err){
      var response = {
        "msg" : "Error borrando cuenta"
      }
      res.status(500);
    } else {
      var response = {
        "msg" : "Cuenta borrada " + IdCuenta
      }
    }
    res.send(response);
  }
  )
}


module.exports.getAccountById = getAccountById;
module.exports.createAccount = createAccount;
module.exports.createAccountToUser = createAccountToUser;
module.exports.getMovementsByIdV2 = getMovementsByIdV2;
module.exports.getMovementsV2 = getMovementsV2;
module.exports.createMovementsV1 = createMovementsV1;
module.exports.createMovementsV2 = createMovementsV2;
module.exports.deleteMovementsV2 = deleteMovementsV2;
module.exports.deleteAccountsV2 = deleteAccountsV2;
