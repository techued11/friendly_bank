const requestJson = require('request-json');
const io = require ('../io');
const crypt = require ('../crypt');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuaag11ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function loginV1 (req, res) {
  console.log("POST /apitechu/v1/login");

  console.log(req.body.email);
  console.log(req.body.pass);

  var users = require('../usuarios.json');

  var userExist = false;
  var passOK = false;
  var userId = 0;
  var index = 0;

  console.log("Login de usuario " + req.body.email);
  console.log("Contraseña " + req.body.pass);

  for (var i = 0; i < users.length; i++) {
    console.log("comparando " + users[i].email + " y " +  req.body.email);
    if (users[i].email == req.body.email) {
      console.log("Mail existe " + req.body.email);
      userExist = true;
      userId = users[i].id;

      if (users[i].pass == req.body.pass)
      {
        passOK = true;
        index = i;
      }
      break;
    }
  }


  if (userExist == false)
  {
    console.log("EMail no existe");
    msg = "Usuario no Existe"
  }
  else {
    console.log("EMail existe");
    if (passOK){
        console.log("Password OK");
        msg = "Usuario logado: " + userId;
        users[index].logged = true;

    }
    else {
      console.log("Password NOK");
      msg = "Password no OK";
    }
  }


  io.writeUserDataToFile(users);

if (passOK) {
   res.send({"msg" : msg, "logged": true});
}
else {
res.send({"msg" : msg});

  }
}

function loginV2 (req, res) {
  console.log("POST /apitechu/v2/login");


  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente httpClient creado para login V2");

  console.log(req.body.email);
  console.log(req.body.password);

  var email = req.body.email;
  var password = req.body.password;


  var query = 'q={"email":"' + email + '"}';

  console.log("La consulta es " + query);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function (err, resMLab, body){
      if (err){
          var response = {
            "msg" : "Error obteniendo usuario"
          }
          res.send(response);
        } else {
                console.log(" ---- 1. CONTENIDO BODY[0]----");
                console.log(body[0]);
                if (body[0] != null) {
                  console.log(" ---- 2. CONTENIDO DE BODY[0] ----");
                  console.log(body[0]);
                  console.log("Usuario encontrado");
                  //console.log("Password -> " + body[0].password);
                  var UserId = body[0].UserId;
                  console.log(body[0].UserId);
                  if (crypt.checkPassword(password,body[0].password)){
                  console.log("Password OK");
                  var putBody = '{"$set":{"logged":true}}';
                  httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                    function (err, resMLab, body){
                      if (err){
                        var response = {
                          "msg" : "Error añadiendo propiedad logged",
                        }

                      } else {
                        var response = {
                        "msg" : "Usuario con propiedad logged",
                        "UserId" : UserId
                        }
                       }
                    res.send(response);
                  }
                )
              } else{
                console.log("Contraseña no es correcta");
                var response = {
                  "msg" : "Usuario y/o password no correcto"
                }
                //res.send(response);
                res.send(response);
              }
           }  // if (body[0] != null)
           else {
             console.log("Usuario no encontrado");
             var response = {
               "msg" : "Usuario y/o password no correcto"
             }
             res.send(response);
           }
        }
      //res.send(response);
    }
  )
}

function logoutV1 (req, res) {
  console.log("POST /apitechu/v1/logout/:id");

  var users = require('../usuarios.json');

  var userLogged = false;
  var index = 0;
  var logged = false;

  console.log("Id usuario logout " + req.params.id);


  for (var i = 0; i < users.length; i++) {
    console.log("comparando " + users[i].id + " y " +  req.params.id);
    if (users[i].id == req.params.id) {
      console.log("Usuario encontrado: " + req.params.id);
      if (users[i].logged == true){
        //console.log("Usuario logado");
        userLogged = true;
        index = i;
      }
      else {
        userLogged = false;
      }
      //userId = users[i].id;
      break;
    }

  }


  if (userLogged == false)
  {
    console.log("Usuario no logado");
    msg = "Usuario NO logado";
  }
  else {
    console.log("Usuario logado");
    msg = "Usuario logado userID: " + req.params.id;

    //result = delete users.logged;
    result = delete users[index].logged;

    if (result){
      console.log("Logged borrado");
    }
    else {
      console.log("Logged NO borrado");
    }
  }


  io.writeUserDataToFile(users);


   res.send({"msg" : msg});


  }

  function logoutV2 (req, res) {
  console.log("POST /apitechu/v2/logout/:id");
  var putBody = '{"$unset":{"logged":""}}'
  var UserId = req.params.id;
  var httpClient = requestJson.createClient(baseMLabURL);

  var query = 'q={"UserId":' + UserId + '}';

  console.log("La consulta es " + query);

  httpClient.put("user?" + query + "&" + mLabAPIKey,JSON.parse(putBody),
    function (err, resMLab, body){
      if (err){
        var response = {
          "msg" : "Error quitando propiedad logged"
        }

        } else {
          var response = {
            "msg" : "Propiedad Logged quitada al usuario",
            "UserId" :  UserId
          }
        }
        res.send(response);
    }
  )

  }


  //io.writeUserDataToFile(users);

  //console.log("Proceso creacion usuario terminado");

  //res.send({"msg": "usuario creado"});


module.exports.loginV2 = loginV2;
module.exports.logoutV1 = logoutV1;
module.exports.logoutV2 = logoutV2;
module.exports.loginV1 = loginV1;
