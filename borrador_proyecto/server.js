require('dotenv').config();
const express = require ('express');
const app = express();
// iniciar el framework

app.use(express.json());
//indica preprocesamiento en json (implica que el content-type es json por defecto). Con esto entiende que lo q se le manda en el body es un json

const port = process.env.PORT || 3000;

// si no hay puerto de escucha definido, nuestra API escuchara en el puerto 3000


const userController = require ('./controllers/UserController');
const authController = require ('./controllers/AuthController');
const accountController = require ('./controllers/AccountController');

var enableCORS = function(req,res,next){

  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
  res.set("Access-Control-Allow-Headers", "Content-Type");

next();
}

app.use(enableCORS);

app.listen(port);

//inicia el servidor en escucha en el puerto

console.log("API escuchando en el puerto " + port);

app.get("/apitechu/v1/hello",

function (req, res) {
  console.log("GET /apitechu/v1/hello");

  //res.send('Hola desde APITechu');
  //res.send('{"msg" : "Hola desde APITechu"}');
  res.send({"msg" : "Hola desde APITechu"});  // en formato JSON
}
)

app.get("/apitechu/v1/users", userController.getUsersV1);
app.get("/apitechu/v2/users", userController.getUsersV2);
app.get("/apitechu/v2/users/:id", userController.getUsersByIdV2);
app.get("/apitechu/v2/accounts/:id", accountController.getAccountById);
app.post("/apitechu/v2/accounts", accountController.createAccount);
app.post("/apitechu/v1/users", userController.createUserV1);
app.post("/apitechu/v2/users", userController.createUserV2);
app.post("/apitechu/v3/users", userController.createUserV3);
app.delete("/apitechu/v1/users/:id",userController.deleteUsersV1);
app.delete("/apitechu/v2/users/:id",userController.deleteUsersV2);
app.post("/apitechu/v2/login", authController.loginV2);
app.post("/apitechu/v1/logout/:id", authController.logoutV1);
app.post("/apitechu/v2/logout/:id", authController.logoutV2);
app.get("/apitechu/v2/movements", accountController.getMovementsV2);
app.get("/apitechu/v2/movements/:id", accountController.getMovementsByIdV2);
app.post("/apitechu/v1/movements/:id", accountController.createMovementsV1);
app.post("/apitechu/v2/movements/:id", accountController.createMovementsV2);
app.delete("/apitechu/v2/movements/:id",accountController.deleteMovementsV2);
app.delete("/apitechu/v2/accounts/:id",accountController.deleteAccountsV2);
app.post("/apitechu/v1/monstruo/:p1/:p2",

function (req, res) {
  console.log ("POST /apitechu/v1/monstruo/p1/:p2");

  console.log ("Parametros");
  console.log (req.params);

  console.log ("Query strings");
  console.log (req.query);

  console.log ("Headers");
  console.log (req.headers);

  console.log ("Body");
  console.log (req.body);

  }
)
